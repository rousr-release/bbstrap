#if !defined _DEBUG
#pragma comment(lib, "sfml-graphics-s.lib")
#pragma comment(lib, "sfml-window-s.lib") 
#pragma comment(lib, "sfml-system-s.lib")
#pragma comment(lib, "sfml-main.lib")
#else 
#pragma comment(lib, "sfml-graphics-s-d.lib")
#pragma comment(lib, "sfml-window-s-d.lib")
#pragma comment(lib, "sfml-system-s-d.lib")
#pragma comment(lib, "sfml-main-d.lib")
#endif

#include "bbStrap/corecontext.h"

#include "bbStrap/debug.h"
#include "bbStrap/input.h"
#include "bbStrap/filesystem.h"
#include "bbStrap/resources.h"
#include "bbStrap/scenemanager.h"
#include "bbStrap/bbimgui.h"

#include "example/example.h"

#include "sandbox.h"

#ifndef SUBMISSION
#define ResourceRoot ("./data")
#else 
#define ResourceRoot ("./data/")
#endif


int main()
{
#if defined(SANDBOX)
    if (!sandbox())
        return 0;
#endif
    sr::EventSubs events; 

    CoreCtx.createComponent<CDebugComponent>();
    CoreCtx.createComponent<CInputComponent>();
    CoreCtx.createComponent<CFileSystem>();
    CoreCtx.createComponent<CResources>(ResourceRoot, "gamedata.json");
    CoreCtx.createComponent<CBBImGui>();
    CoreCtx.createComponent<CSceneManager>();
    CoreCtx.createComponent<Example>();

    CoreCtx.initialize(sf::Vector2u(1280, 720));

    events.push_back(Input.registerKey(sf::Keyboard::Escape, [](sf::Keyboard::Key _key, sr::InputEvent _ev) {
        CoreCtx.signalShutdown();
        return sr::InputReply::Pass;
    }));

    events.push_back(Input.registerKey(sf::Keyboard::T, [](sf::Keyboard::Key _key, sr::InputEvent _ev) {
        Debug.log("Testing.");
        return sr::InputReply::Pass;
    }));

    while (CoreCtx.tick()) { ; }

    return CoreCtx.status();
}
