#pragma once

#include "../bbStrap/scene.h"

class CComputer;
class CMonitor;
class CDiskette;
class CBios;

class CEntityManager;
class CDrawBatcher;

class CGameScene : public CScene {
public:
    
    void enter(std::shared_ptr<CScene> _prevScene) override;
    void exit(std::shared_ptr<CScene>  _nextScene) override;

private:

    bool mInitialized = false;

    std::shared_ptr<CDrawBatcher> mDrawBatcher;

    sr::PtrVector<CComputer> mComputers;
    sr::PtrVector<CMonitor> mMonitors;
    sr::PtrVector<CDiskette> mDiskettes;
    sr::PtrVector<CBios> mBioses;
};