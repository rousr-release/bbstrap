#pragma once

#include "../bbStrap/corecontextcomponent.h"

#include "../bbStrap/bbimgui.h"

class Example : public CCoreContextComponent {
public:
    using CCoreContextComponent::CCoreContextComponent;

    void initialize() override;
    void tick() override;

private:

    sr::EventSubs mEvents;
};