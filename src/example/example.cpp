#include "example.h"

#include "../bbStrap/corecontext.h"
#include "../bbStrap/scenemanager.h"

#include "gamescene.h"

void Example::initialize() {
    auto sceneManager(context()->get<CSceneManager>());
    if (sceneManager == nullptr) {
        throw std::exception("example component requires a scene manager");
    }

    sceneManager->addScene("Example", std::make_shared<CGameScene>(), true);

    mEvents.push_back(bbImGui.onImGui([]() {
        ImGui::Begin("Example");
        ImGui::Text("This ImGui window is spawned from the example Context Component");
        ImGui::End();
    }));
}

void Example::tick() {

}
