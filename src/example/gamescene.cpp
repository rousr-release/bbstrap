#include "gamescene.h"

#include "example.h"

#include "../bbStrap/drawbatcher.h"
#include "../bbStrap/entitycomponentshape.h"

#include <SFML/Graphics/Shape.hpp>

void CGameScene::enter(std::shared_ptr<CScene> _prevScene) {
    if (!mInitialized) {
        mDrawBatcher = createComponent<CDrawBatcher>();
        mDrawBatcher->setRenderTarget(static_cast<sf::RenderTarget*>(CoreCtx.renderWindow()));

        std::shared_ptr<EntityShapeComponent> shapeComponent(spawnEntity<IEntity>()->createComponent<TEntityShapeComponent<sf::CircleShape>>(100.0f));
        shapeComponent->shape().setPosition(sf::Vector2f((1280.0f - 100.0f) / 2, (720.0f - 100.0f) / 2));
        shapeComponent->shape().setFillColor(sf::Color::Green);

        shapeComponent = spawnEntity<IEntity>()->createComponent<TEntityShapeComponent<sf::RectangleShape>>(sf::Vector2f(100.0f, 50.0f));
        shapeComponent->shape().setPosition(sf::Vector2f(100.0f, 200.0f));
        shapeComponent->shape().setFillColor(sf::Color::Red);

        bbImGui.onDraw([this](CBBImGui& _imgui) {
            mDrawBatcher->batch(_imgui.shared_from_this(), 1000);
        });

        mInitialized = true;
    }
}

void CGameScene::exit(std::shared_ptr<CScene>  _nextScene) {

}
