#pragma once

// uncomment to have a sandbox of code to play with right when main starts.
// return false to kill main when done
//#define SANDBOX

#if defined SANDBOX
bool sandbox();
#endif 