#include "entitycomponentsprite.h"

#include "entity.h"
#include "drawbatcher.h"
#include "scene.h"


void EntityComponentSprite::onConstruct() {
    auto batcher(entity().scene().getComponent<CDrawBatcher>());
    
    entity().subscribePhase(EEntityPhase::Draw, shared_from_this(), [this, batcher]() {
        if (mSprite != nullptr) {
            batcher->batch<sf::Sprite>(mSprite, 0);
            // batcher->batch<EntityComponentSprite>(shared_from_this());
        }
    });

    entity().subscribePhase(EEntityPhase::Tick, shared_from_this(), [this]() {
        // todo: tick stuff            
    });
}

void EntityComponentSprite::onConstruct(std::shared_ptr<TResource<sf::Texture>> _texture) {
    mSprite = std::make_shared<sf::Sprite>(*(*_texture.get()));
    mSprite->setPosition(sf::Vector2f(200, 200));
    onConstruct();
}
