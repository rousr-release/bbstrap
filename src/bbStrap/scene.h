#pragma once

#include "core.h"

#include "TECS.h"
#include "scenecomponent.h"
#include "entity.h"
#include "sceneroot.h"

class IEntity;

class CScene : public TECS<CScene, ISceneComponent> {
public:
    CScene();
    virtual ~CScene() { }
    
    virtual void enter(std::shared_ptr<CScene> _prevScene) { ; }
    virtual void exit(std::shared_ptr<CScene>  _nextScene) { ; }

    void onComponentCreated(const std::shared_ptr<ISceneComponent>& _component) override;
    
    template <typename TEntity, typename... Args>
    std::shared_ptr<TEntity> spawnEntity(Args&&... _args) {
        return mRoot->createChild<TEntity>(std::forward<Args>(_args)...);
    }

    void tick();
    void draw();

protected:
    std::function<void(const std::function<void(const std::shared_ptr<ISceneComponent>&)>&&)> componentLoop;
    std::shared_ptr<SceneRoot> mRoot;
};

