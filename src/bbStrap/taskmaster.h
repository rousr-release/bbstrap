//todo: implement TaskMaster
//
// what is it: taskmaster is a thread manager that can farm out simple tasks on multiple, available threads.
//             when a task is complete it can call a callback on the thread that began the task, and then uses the thread for the next task
#pragma once

#include "corecontextcomponent.h"

class CTaskMaster : TContextComponentStatic<CTaskMaster> {
public:


private:

};
