#pragma once

#include "core.h"

#include "TECS.h"
#include "objectpool.h"
#include "parent.h"

#include "eventdispatchermulti.h"
#include "eventdispatchersorted.h"

#include "entity_internal.h"
#include "entitymsg.h"

class IEntityComponent;
class CScene;

class IEntity : public TECS<IEntity, IEntityComponent>, public TSharedParent<IEntity> {
public:
    virtual ~IEntity() = default;

    virtual void onConstruct() { ; }
    virtual bool isActive() { return mActive; }
    virtual bool isVisible() { return mVisible; }

    CScene& scene() { return *mScene; }

    ////
    // Event Handling
    virtual void subscribePhase(EEntityPhase _phase, const std::shared_ptr<IEntityComponent>& _component, const std::function<void()>& _func);
    
    // Component Events
    sr::EventSubPtr subscribeEvent(const std::string& _event, std::function<EntityReply(IEntityMsg&)>& _func) { return mEntityEvents.subscribe(_event, _func); }
    void dispatchEvent(const std::string& _event, IEntityMsg& _msg) const;
    // ~Event Handling

    ////    
    // Entity Hierarchy
    template <typename TChildEntity, typename ...Args>
    std::shared_ptr<TChildEntity> createChild(Args&&... _args) {
        auto newEntity(std::make_shared<TChildEntity>());
        addChild(newEntity); 
        newEntity->onConstruct(std::forward<Args>(_args)...);
        return newEntity;
    }

    void addChild(std::shared_ptr<IEntity> _child) override;
    
protected:
    bool mActive = true;
    bool mVisible = true;

    void dispatchPhase(EEntityPhase _phase);
    void setScene(CScene* _scene) { mScene = _scene; }

    // Components passing events
    TMultiEventDispatcher<std::string, EntityReply(IEntityMsg&)> mEntityEvents;

    // my components that are subscribed to this phase
    TMultiEventDispatcher<EEntityPhase, void()> mEntityComponentPhases;
    std::map<IEntityComponent*, sr::EventSubs> mComponentSubscriptions;

    // hold a pointer to the subscription to this phase on our parent
    sr::EventSubPtr mPhaseSubscriptions[static_cast<int>(EEntityPhase::Num)];

private:
    CScene* mScene;
    friend class IEntityComponent;

protected:

    ////
    // TECS
    void onComponentCreated(const std::shared_ptr<IEntityComponent>& _component) override;
    // ~TECS

    // my children that subscribe to this phase
    entityInternal::EntityPhaseDispatcher mEntityPhases;
    
    virtual sr::EventSubPtr subscribePhase(EEntityPhase _phase, const std::shared_ptr<IEntity>& _child);
};
