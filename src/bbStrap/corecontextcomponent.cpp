#include "corecontextcomponent.h"

#include "corecontext.h"

CCoreContextComponent::CCoreContextComponent(std::shared_ptr<CCoreContext> _context)
    : mContext(_context) {
}