#include "resources.h"

#include "filesystem.h"
#include "file.h"
#include "./vendor/rapidjson/document.h"

#include "resourcetexture.h"
#include <SFML/Graphics.hpp>

ContextComponentDefine(CResources);

const std::string CResources::NoFilePath = std::string("");

void CResources::loadResources(const std::string& _filePath) {
    rapidjson::Document resourceDoc;
    auto file(FileSystem.open(mRoot + "//" + _filePath, false));
    
    resourceDoc.Parse(file->asString().c_str());

    const auto& resources(resourceDoc["resources"]);
    
    if (resources.HasMember("textures")) {
        const auto& textures(resources["textures"]);
        for (auto it(textures.MemberBegin()), itEnd(textures.MemberEnd()); it != itEnd; ++it) {
            auto& textureData((*it).value);
            auto& path(textureData["path"]);
            mResources[(*it).name.GetString()] = std::make_shared<TResource<sf::Texture>>(mRoot + "//" + path.GetString());
        }
    }    
}