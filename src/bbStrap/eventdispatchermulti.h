#pragma once

#include "eventdispatcher.h"

template <typename TKey, typename TFuncSig, typename TSubscriber = TEventSubscriber<TFuncSig>, typename TDispatcher = TEventDispatcher<TFuncSig, TSubscriber>>
class TMultiEventDispatcher {
public:
    using TFunc = std::function<TFuncSig>;
    using TSubCRef = const TSubscriber&;

    template <typename... Args>
    sr::EventSubPtr subscribe(const TKey& _key, const TFunc& _func, Args&&... _args) {
        auto& dispatcher(mEventMap[_key]);
        auto subscriber(mEventMap[_key].subscribe(_func, std::forward<Args>(_args)...));
        mSubscriberMap[subscriber.get()] = _key;
        return subscriber;
    }

    void remove(TSubscriber* _ptr) {
        auto itSubscriber(mSubscriberMap.find(_ptr));
        if (itSubscriber == mSubscriberMap.end()) {
            return;
        }

        auto  itDispatcher(mEventMap.find(itSubscriber->second));
        if (itDispatcher == mEventMap.end()) {
            return;
        }

        (itDispatcher->second).remove(_ptr);
    }

    template <typename ...Args>
    void dispatch(const TKey& _key, Args&&... _args) const {
        auto itDispatcher(mEventMap.find(_key));
        if (itDispatcher == mEventMap.end())
            return;

        (itDispatcher->second).dispatch(std::forward<Args>(_args)...);
    }

    void dispatchLambda(const TKey& _key, const std::function<bool(TSubCRef)>& _handler) const {
        auto itDispatcher(mEventMap.find(_key));
        if (itDispatcher == mEventMap.end())
            return;

        (itDispatcher->second).dispatchLambda(_handler);
    }

    template <typename TRet, typename ...Args>
    TRet dispatchReturn(const TKey& _key, const TRet& _returnOnVal, Args&&... _args) const {
        auto itDispatcher(mEventMap.find(_key));
        if (itDispatcher == mEventMap.end())
            return;

        return (itDispatcher->second).dispatchReturn<TRet>(_returnOnVal, std::forward<Args>(_args)...);
    }

    template <typename TRet, typename ...Args>
    TRet dispatchReturnLambda(const TKey& _key, const std::function<bool(const TRet&)> _func, Args&&... _args) const {
        auto itDispatcher(mEventMap.find(_key));
        if (itDispatcher == mEventMap.end())
            return;

        return (itDispatcher->second).dispatchReturnLambda<TRet>(_func, std::forward<Args>(_args)...);
    }

    const TDispatcher* dispatcher(const TKey& _key) {
        auto itDispatcher(mEventMap.find(_key));
        if (itDispatcher == mEventMap.end())
            return;

        return &(itDispatcher->second);
    }

    bool empty(const TKey& _key) const {
        auto itDispatcher(mEventMap.find(_key));
        if (itDispatcher == mEventMap.end())
            return false;

        return (itDispatcher->second).empty();
    }

private:
    template <typename TFuncSig, typename TSub = TSubscriber>
    class TDispatcherHelper : public decltype(TDispatcher) {
    public:
        sr::EventSubPtr subscribe(TMultiEventDispatcher* _parent, const TFunc& _func) {
            mParent = _parent;
            return TDispatcher::subscribe(_func);
        }

    protected:
        void remove(TSubscriber* _ptr) override {
            TEventDispatcher::remove(_ptr);
            if (_parent != nullptr) {
                _parent->remove(this);
            }
        }
    };

    std::map<TKey, TDispatcher> mEventMap;
    std::map<IEventSubscriber*, TKey> mSubscriberMap;
};