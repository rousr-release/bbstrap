#pragma once

#include "core.h"
#include "scenecomponent.h"

#include "corecontext.h"

#include <SFML/Graphics.hpp>

#include "drawbatch.h"

class IDrawBatch;
class IEntityComponentDrawable;
class IEntityComponent;

class CDrawBatcher : public ISceneComponent {
public:
    virtual ~CDrawBatcher() = default;

    void setRenderTarget(sf::RenderTarget* _renderTarget) { mRenderTarget = _renderTarget; }
    sf::RenderTarget* getRenderTarget() const { return mRenderTarget; }

    template <typename TBatchType>
    void batch(const std::shared_ptr<IEntityComponent>& _batchee) { batch(std::static_pointer_cast<TBatchType>(_batchee)); }

    template <typename TBatchType>
    void batch(const std::shared_ptr<IEntityComponent>& _batchee, int32_t _depth) { batch(std::static_pointer_cast<TBatchType>(_batchee), _depth); }

    // try to deduce depth from batchtype if none passed
    template <typename TBatchType>
    void batch(const std::shared_ptr<TBatchType>& _batchee) {
        batch(_batchee, _batchee->depth());
    }
    
    template <typename TBatchType>
    void batch(const std::shared_ptr<TBatchType>& _batchee, int32_t _depth) {
        std::shared_ptr<TDrawBatch<TBatchType>> activeBatch(matchBatch(mActiveBatch, _depth, _batchee) ? std::static_pointer_cast<TDrawBatch<TBatchType>>(mActiveBatch) : nullptr);

        if (activeBatch == nullptr) {
            activeBatch = withdrawBatch<TBatchType>(_depth, _batchee);
            mActiveBatch = activeBatch;
        }

        activeBatch->batch(_batchee);
    }

    void draw() override;

protected:
    template <typename TBatchType>
    std::shared_ptr<TDrawBatch<TBatchType>> withdrawBatch(int32_t _depth, const std::shared_ptr<TBatchType>& _batchee = std::shared_ptr<TBatchType>()) {
        size_t batchType(TDrawBatch<TBatchType>::sType());
        std::shared_ptr<TDrawBatch<TBatchType>> drawbatch(nullptr);

        std::vector<std::shared_ptr<IDrawBatch>>& batches(getBatchesAt(_depth));
        if (!batches.empty() && (_batchee == nullptr || matchBatch(batches.back(), _depth, _batchee))) {
            drawbatch = std::static_pointer_cast<TDrawBatch<TBatchType>>(batches.back());
        }

        if (drawbatch == nullptr) {
            auto& batchList(mFreeBatches[batchType]);
            if (batchList.empty()) {
                drawbatch = std::make_shared<TDrawBatch<TBatchType>>(_depth);
            } else {
                drawbatch = std::static_pointer_cast<TDrawBatch<TBatchType>>(batchList.back());
                batchList.pop_back();
            }

            batches.push_back(drawbatch);
        }

        drawbatch->depth(_depth);
        return std::static_pointer_cast<TDrawBatch<TBatchType>>(drawbatch);
    }

    std::vector<std::shared_ptr<IDrawBatch>>& getBatchesAt(int32_t _depth) {
        auto itDepth(std::upper_bound(mBatches.begin(), mBatches.end(), DepthBatch(), [_depth](const DepthBatch& _left, const DepthBatch& _right) {
            return _depth < _right.first;
        }));

        if (itDepth == mBatches.end()) {
            itDepth = mBatches.insert(itDepth, { _depth, sr::PtrVector<IDrawBatch>() });
        } else {
            if (itDepth != mBatches.begin() && (*(itDepth - 1)).first == _depth) {
                --itDepth;
            } else if ((*itDepth).first != _depth) {
                itDepth = mBatches.insert(itDepth, { _depth, sr::PtrVector<IDrawBatch>() });
            }
        }

        return (*itDepth).second;
    }

    template <typename TBatchType>
    bool matchBatch(const std::shared_ptr<IDrawBatch>& _batch, int32_t _testDepth, const std::shared_ptr<TBatchType>& _testBatchee) {
        if (_batch == nullptr || _testBatchee == nullptr)
            return false;

        if (_batch->type() != TDrawBatch<TBatchType>::sType() || _batch->depth() != _testDepth)
            return false;

        auto activeBatch(std::static_pointer_cast<TDrawBatch<TBatchType>>(_batch));
        return activeBatch->isCompatible(_testBatchee);
    }

private:
    sr::PtrVector<IEntityComponentDrawable> mDrawables;
    sf::RenderTarget* mRenderTarget;

    std::shared_ptr<IDrawBatch> mActiveBatch;
    
    using DepthBatch = std::pair<int32_t, sr::PtrVector<IDrawBatch>>;
    std::vector<DepthBatch> mBatches;
    std::map<size_t, sr::PtrVector<IDrawBatch>> mFreeBatches;
};

