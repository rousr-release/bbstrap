#pragma once

#include "entitycomponent.h"

#include "corecontext.h"
#include "entity.h"
#include "scene.h"
#include "drawbatcher.h"

#include <SFML/Graphics/Shape.hpp>

class EntityShapeComponent : public IEntityComponent, public sf::Drawable {
public:
    void onConstruct(std::unique_ptr<sf::Shape>&& _shape) {
        mShape = std::move(_shape);
        auto batcher(entity().scene().getComponent<CDrawBatcher>());
        entity().subscribePhase(EEntityPhase::Draw, shared_from_this(), [this, batcher]() {
            batcher->batch<EntityShapeComponent>(shared_from_this());
        });
    }

    //// sf::Drawable
    void draw(sf::RenderTarget& _target, sf::RenderStates _states) const override {
        _target.draw(*mShape);
    }
    

    const sf::Shape& shape() const { return *mShape.get(); }
    sf::Shape& shape() { return *mShape.get(); }

    int32_t depth() const { return mDepth; }

private:
    std::unique_ptr<sf::Shape> mShape;
    int mDepth = 0;
};

template <typename TShape>
class TEntityShapeComponent : public EntityShapeComponent {
public:
    template <typename ...Args>
    void onConstruct(Args&&... _args) {
        EntityShapeComponent::onConstruct(std::move(std::make_unique<TShape>(std::forward<Args>(_args)...)));
    }
};
