#pragma once

namespace sr {
    template <typename T> using PtrVector = std::vector<std::shared_ptr<T>>;
    template <typename T> using UPtrVector = std::vector<std::unique_ptr<T>>;
    template <typename T> using WPtrVector = std::vector<std::weak_ptr<T>>;


    // from: https://stackoverflow.com/questions/15549722/double-inheritance-of-enable-shared-from-this
    struct virtual_enable_shared_from_this_base : std::enable_shared_from_this<virtual_enable_shared_from_this_base> {
        virtual ~virtual_enable_shared_from_this_base() {}
    };

    template<typename T>
    struct virtual_enable_shared_from_this : virtual virtual_enable_shared_from_this_base {
        std::shared_ptr<T> shared_from_this() {
            return std::dynamic_pointer_cast<T>(virtual_enable_shared_from_this_base::shared_from_this());
            //return std::static_pointer_cast<T>(virtual_enable_shared_from_this_base::shared_from_this());
        }

        template<typename U>
        std::shared_ptr<U> shared_from_this() {
            return std::dynamic_pointer_cast<U>(virtual_enable_shared_from_this_base::shared_from_this());
            //return std::static_pointer_cast<U>(virtual_enable_shared_from_this_base::shared_from_this());
        }

        template<>
        std::shared_ptr<T> shared_from_this() {
            return std::dynamic_pointer_cast<T>(virtual_enable_shared_from_this_base::shared_from_this());
            //return std::static_pointer_cast<T>(virtual_enable_shared_from_this_base::shared_from_this());
        }
    };

}