#include "sceneroot.h"


void SceneRoot::onConstruct(CScene* _scene) {
    IEntity::setScene(_scene);
}

void SceneRoot::pretick() { 
    mEntityPhases.dispatch(EEntityPhase::Pretick); 
}

void SceneRoot::tick() { 
    mEntityPhases.dispatch(EEntityPhase::Tick); 
}

void SceneRoot::posttick() { 
    mEntityPhases.dispatch(EEntityPhase::Posttick); 
}

void SceneRoot::predraw() { 
    mEntityPhases.dispatch(EEntityPhase::Predraw); 
}

void SceneRoot::draw() { 
    mEntityPhases.dispatch(EEntityPhase::Draw); 
}

void SceneRoot::postdraw() { 
    mEntityPhases.dispatch(EEntityPhase::Postdraw); 
}