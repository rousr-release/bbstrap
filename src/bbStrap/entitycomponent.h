#pragma once

#include "entity.h"

class IEntityComponent : public sr::virtual_enable_shared_from_this<IEntityComponent> {
public:
    IEntityComponent() { ; }
    virtual ~IEntityComponent() = default;

    virtual void onConstruct() { ; }
    IEntity& entity() const { return *mEntity.lock(); }

private:
    std::weak_ptr<IEntity> mEntity;
    friend class IEntity;
};
