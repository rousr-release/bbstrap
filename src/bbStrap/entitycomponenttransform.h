#pragma once

#include "entitycomponent.h"

#include <SFML/Graphics/Transform.hpp>

// wrapper for sf::Transform with callbacks
class EntityComponentTransform : public IEntityComponent {
public:

private:
    sf::Transform mTransform;
};
