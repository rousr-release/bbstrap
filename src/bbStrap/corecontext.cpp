#include "corecontext.h"

#include "SFML/Graphics.hpp"

#include "corecontextcomponent.h"

std::shared_ptr<CCoreContext> CCoreContext::mInstance;

CCoreContext::CCoreContext() {

}

CCoreContext::~CCoreContext() {

}

CCoreContext& CCoreContext::instance() {
    if (mInstance == nullptr) {
        mInstance = std::shared_ptr<CCoreContext>(new CCoreContext());
    }

    return *mInstance;
}

void CCoreContext::initialize(const sf::Vector2u& _size, const std::string& _title) {
    mWindow = std::make_unique<sf::RenderWindow>(sf::VideoMode(_size.x, _size.y), _title, sf::Style::None);
    
    mCursorType = sf::Cursor::Arrow;
    mCursor.loadFromSystem(mCursorType);
    mWindow->setMouseCursor(mCursor);
    
    mInitialized = true;

    mTicks.clear();
    mDraws.clear();

    auto components(mComponents);
    mComponents.clear();

    for (auto& component : components) {
        attachComponentInternal(component);
    }
}

void CCoreContext::attachComponentInternal(std::shared_ptr<CCoreContextComponent> _component) {
    mComponents.push_back(_component);
    if (mInitialized) {
        _component->initialize();

        sr::ComponentFlags flags(_component->flags());
        if ((flags & sr::ComponentFlags::Tick) == sr::ComponentFlags::Tick) {
            mTicks.push_back(_component);
        }

        if ((flags & sr::ComponentFlags::Draw) == sr::ComponentFlags::Draw) {
            mDraws.push_back(_component);
        }
    }
}

bool CCoreContext::tick() {
    sf::RenderWindow& window(*mWindow);
    if (!window.isOpen()) {
        signalShutdown();
    }

    sf::Event event;
    while (window.pollEvent(event)) {
        if (event.type == sf::Event::Closed)
            signalShutdown();

        mEventProcessors.dispatch(event);

        auto itDispatcher(mEventDispatchers.find(event.type));
        if (itDispatcher != mEventDispatchers.end()) {
            auto& dispatcher(itDispatcher->second);
            
            dispatcher.dispatchLambda([&event](const auto& _sub)->bool {
                return _sub.callReturn<bool>(event);
            });
        }
    }

    if (mShutdown) {
        return false;
    }

    for (auto& component : mTicks)
        component->tick();
     
    
    window.clear();
    for (auto& component : mTicks)
        component->draw();
    window.display();

    return true;
}

sf::Vector2u CCoreContext::screenSize() const {
    const sf::RenderWindow& window(*mWindow);
    return window.getSize();
}

std::shared_ptr<IEventSubscriber> CCoreContext::subscribeEvent(const sf::Event::EventType& _type, const std::function<bool(sf::Event& _event)>& _func) {
    return mEventDispatchers[_type].subscribe(_func);
}

std::shared_ptr<IEventSubscriber> CCoreContext::subscribeEvent(const std::function<bool(sf::Event& _event)>& _func) {
    return mEventProcessors.subscribe(_func);
}

void CCoreContext::setMouseCursor(const sf::Cursor::Type& _type) {
    if (mCursorType != _type) {
        mCursor.loadFromSystem(_type);
        renderWindow()->setMouseCursor(mCursor);
        mCursorType = _type;
    }
}