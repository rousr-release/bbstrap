#pragma once

#include "core.h"

// todo: more research - this doesn't work because we're accessing the base class's id not the "real" id of the instance in the below functions
//
//template <typename TBaseClass>
//struct ClassTyper {
//    static size_t typeId() {
//        return reinterpret_cast<size_t>(&typeId);
//    }
//};

namespace sr {

    template <typename TClass, typename TBaseClass>
    TClass *dynamicCast(TBaseClass* _ptr) {
        //return ClassTyper<TBaseClass>::typeId() == ClassTyper<TClass>::typeId() ? reinterpret_cast<TClass*>(_ptr) : nullptr;
        return dynamic_cast<TClass*>(_ptr);
    }

    template <typename TClass, typename TBaseClass>
    std::shared_ptr<TClass> dynamicPtrCast(const std::shared_ptr<TBaseClass>& _ptr) {
        //return ClassTyper<TBaseClass>::typeId() == ClassTyper<decltype(_ptr)>::typeId() ? std::reinterpret_pointer_cast<TClass>(_ptr) : nullptr;
        return std::dynamic_pointer_cast<TClass>(_ptr);
    }
}