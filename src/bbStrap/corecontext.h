#pragma once
#include "core.h"

#include <SFML/Window.hpp>

#include "eventdispatcher.h"
#include "classtype.h"

namespace sf {
    class RenderWindow;
    class CircleShape;
}

class CCoreContextComponent;

class ISFMLEventHandler {
public:
    virtual bool handleSFMLevent(sf::Event& _event) = 0;
    virtual ~ISFMLEventHandler() = default;
};

class CCoreContext {
public:
    ~CCoreContext();
    static CCoreContext& instance();

    int status() const { return 0; }

    void initialize(const sf::Vector2u& _size, const std::string& _title = "Rousr SFML");
    bool tick();

    template <typename TComponent, typename ...Args>
    std::shared_ptr<TComponent> createComponent(Args&& ...args) {
        auto newComponent(std::make_shared<TComponent>(mInstance, std::forward<Args>(args)...));
        attachComponentInternal(newComponent);
        return newComponent;
    }

    template <typename TComponent>
    std::shared_ptr<TComponent> get() {
        auto itFind(std::find_if(mComponents.begin(), mComponents.end(), [](const std::shared_ptr<CCoreContextComponent>& _component) {
            return sr::dynamicPtrCast<TComponent>(_component) != nullptr;
        }));
        if (itFind != mComponents.end()) {
            return sr::dynamicPtrCast<TComponent>(*itFind);
        }

        return nullptr;
    }

    void signalShutdown() { mShutdown = true; }

    sf::Vector2u screenSize() const;
    sf::RenderWindow* renderWindow() const { return mWindow.get(); }
    sr::EventSubPtr subscribeEvent(const sf::Event::EventType& _type, const std::function<bool(sf::Event& _event)>& _func);
    sr::EventSubPtr subscribeEvent(const std::function<bool(sf::Event& _event)>& _func);

    void setMouseCursor(const sf::Cursor::Type& _cursorType);

// Functionality
private:

    bool mInitialized = false;
    bool mShutdown = false;

    std::unique_ptr<sf::RenderWindow> mWindow;
    std::unique_ptr<sf::CircleShape> mShape;

    sr::PtrVector<CCoreContextComponent> mComponents;
    
    sr::PtrVector<CCoreContextComponent> mTicks;
    sr::PtrVector<CCoreContextComponent> mDraws;

    void attachComponentInternal(std::shared_ptr<CCoreContextComponent>  _component);

    using TSFMLEventDispatcher = TEventDispatcher<bool(sf::Event&)>;

    TSFMLEventDispatcher mEventProcessors;
    std::map<sf::Event::EventType, TSFMLEventDispatcher> mEventDispatchers; // handle specific events

// Singleton Stuff
private:
    CCoreContext();
    CCoreContext(const CCoreContext&) = delete;
    CCoreContext(const CCoreContext&&) = delete;
    CCoreContext& operator=(const CCoreContext&) = delete;
    CCoreContext& operator=(const CCoreContext&&) = delete;

    sf::Cursor::Type mCursorType;
    sf::Cursor mCursor;

private:
    static std::shared_ptr<CCoreContext> mInstance;
};

#define CoreCtx (CCoreContext::instance())