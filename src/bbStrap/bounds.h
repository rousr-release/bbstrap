#pragma once

template <typename T, int TDim = 2>
struct Bounds {
    T min[TDim];
    T max[TDim];

    Bounds() {
        std::memset(min, 0, sizeof(T) * TDim);
        std::memset(max, 0, sizeof(T) * TDim);
    }
};