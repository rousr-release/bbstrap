#pragma once

#include "core.h"

#include "corecontext_types.h"

class CCoreContext;

class CCoreContextComponent {
public:
    CCoreContextComponent(std::shared_ptr<CCoreContext> _context);
    virtual ~CCoreContextComponent() { ; }
    
    virtual void initialize() { ; }
    virtual sr::ComponentFlags flags() const { return sr::ComponentFlags::Default; }

    virtual void tick() { ; }
    virtual void draw() { ; }

protected:
    std::shared_ptr<CCoreContext> context() const { return mContext; }

private:
    std::shared_ptr<CCoreContext> mContext;
};

template <typename TComponent>
class TContextComponentStatic : public CCoreContextComponent, public std::enable_shared_from_this<TComponent> {
public:
    using CCoreContextComponent::CCoreContextComponent;

    void initialize() override {
        initializeContextComponent(TContextComponentStatic<TComponent>::shared_from_this());
    }

    void initializeContextComponent(std::shared_ptr<TComponent> _shared_this) {
        mContextComponent = _shared_this;
    }

    static TComponent& contextComponent() {
        if (mContextComponent == nullptr) {
            // make sure you call Base::initialize(); if you override void initialize()
            throw std::exception("Context Component not Initialized - check for missing Base::initialize()");
        }
        return *mContextComponent;
    }

protected:
    using Base = TContextComponentStatic<TComponent>;

private:
    static std::shared_ptr<TComponent> mContextComponent;
};

#define ContextComponentDefine(TComponent) std::shared_ptr<TComponent> TContextComponentStatic<TComponent>::mContextComponent;