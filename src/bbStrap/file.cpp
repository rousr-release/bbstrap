////
// otb.cpp
//  babyjeans
//
// Main OutsideTheBox header for the DLL
//  OutsideTheBox contains 3 extensions:
//   *  The original outsideTheBox: a simple extension that reads/writes files outside of the gamemaker sandbox
//   *  Borderless Toggle: a gms2 specialty for toggling borderless and bordered windowed modes
//   *  MemInfo: a simple utility that reads memory usage data for the proecss
//
//  This class is basically all three, though meminfo is split into its own object since it's a bit convoluted.
////
#include "core.h"
#include <fstream>

#include "file.h"

#if defined _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

const std::string& CFile::None = "";

namespace {
    std::string getExpandedPath(const std::string& _path) {
        const int cnSize(1024);
        char expandedPath[cnSize];
        memset(expandedPath, 0, cnSize);
        std::string ret(expandedPath);
#if defined _WIN32
        ExpandEnvironmentStringsA(_path.c_str(), expandedPath, cnSize);
        ret = expandedPath;
#elif defined __GNUC__
        wordexp_t p;
        wordexp(_path.c_str(), &p, 0);
        char** w(p.we_wordv);
        ret = "";
        for (size_t i(0); i < p.we_wordc; ++i) {
            if (i != 0)
                ret += " ";
            ret += w[i];
        }
#endif 
        return ret;
    }
}


class IFileStream {
public:
    virtual ~IFileStream() { ; }
    virtual bool is_open() const = 0;
};


namespace {
    class OFStream : public IFileStream {
    public:
        template <typename... Args>
        OFStream(Args&&... _args) {
            mFile = std::ofstream(std::forward<Args>(_args)...);
        }

        bool is_open() const override { return mFile.is_open(); }

    private:
        std::ofstream mFile;
    };

    class IFStream : public IFileStream {
    public:
        template <typename... Args>
        IFStream(Args&&... _args) {
            mFile = std::ifstream(std::forward<Args>(_args)...);
        }

        bool is_open() const override { return mFile.is_open(); }
        std::string asString() {
            return std::string((std::istreambuf_iterator<char>(mFile)), std::istreambuf_iterator<char>());
        }
    private:
        std::ifstream mFile;
    };
}

CFile::CFile(const std::string& _filePath, bool _createIfMissing) : mFilepath(_filePath) {
    auto expandedPath(getExpandedPath(_filePath));
    mFile = std::make_shared<::IFStream>(expandedPath, std::ios::binary);
    if (!mFile->is_open()) {
        if (!_createIfMissing) {
            mFile.reset();
            return;
        }

        mFile = std::make_shared<::OFStream>(expandedPath, std::ios::binary);
        if (!mFile->is_open())
            mFile.reset();
    }
}

CFile::~CFile() {
    mFile.reset();
}

bool CFile::is_open() const {
    return mFile != nullptr && mFile->is_open();
}

const std::string& CFile::readLine() {
    if (is_open()) {

    }

    return None;
}

const std::string& CFile::asString() {
    if (mFileBuffer.empty()) {
        std::shared_ptr<::IFStream> ifFile(std::dynamic_pointer_cast<::IFStream>(mFile));
        if (ifFile != nullptr) {
            mFileBuffer = ifFile->asString();
        }
    }

    return mFileBuffer;
}