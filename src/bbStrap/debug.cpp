#include "debug.h"

ContextComponentDefine(CDebugComponent);

void CDebugComponent::log(const std::string& _log) {
    std::cout << _log << std::endl;
}
