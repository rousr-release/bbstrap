////
// otb.h
//  babyjeans
//
// Main OutsideTheBox header for the DLL
//  OutsideTheBox contains 3 extensions:
//   *  The original outsideTheBox: a simple extension that reads/writes files outside of the gamemaker sandbox
//   *  Borderless Toggle: a gms2 specialty for toggling borderless and bordered windowed modes
//   *  MemInfo: a simple utility that reads memory usage data for the proecss
//
//  This class is basically all three, though meminfo is split into its own object since it's a bit convoluted.
////
#pragma once  

class IFileStream;

class CFile {
    static const std::string& None;

public:
    virtual ~CFile();

    const std::string& filepath() const { return mFilepath; }
    bool is_open() const;

    const std::string& asString();
    const std::string& readLine();

protected:
    CFile(const std::string& _filepath, bool _createIfMissing);

private:

    std::string mFileBuffer;
    std::string mFilepath;
    std::shared_ptr<IFileStream> mFile;
};

//    size_t             FileOpen(const std::string& _fileName, bool _createIfMissing);
//    void               FileClose(size_t   _fileIndex);
//    const std::string& FileGetName(size_t _fileIndex);

