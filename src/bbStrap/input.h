#pragma once

#include "corecontextcomponent.h"

#include <SFML/Window/Keyboard.hpp>

#include "eventdispatcher.h"

namespace sr {
    enum class InputEvent : uint32_t {
        None = 0,

        // Only register one or the other, Down is called on Pressed.
        Pressed,
        Down, 
        
        // Only register one or the other, Up is called on Released.
        Released,
        Up,

        DefaultRegister = Pressed
    };

    enum class InputKeyState : uint32_t {
        None = 0,

        JustChanged = (1 << 0),
        Pressed     = (1 << 1),
        Down = Pressed,

        Released    = (1 << 2),
        Up = Released,
    };

    enum class InputReply {
        Pass = 0,
        Consume = 1,
    };
}

class CInputComponent : public TContextComponentStatic<CInputComponent> {
public:
    
    using FInputCallback = std::function<sr::InputReply(sf::Keyboard::Key, sr::InputEvent)>;
    using TKeyDispatcher = TEventDispatcher<sr::InputReply(sf::Keyboard::Key, sr::InputEvent)>;
public:
    using Base::Base;

    sr::ComponentFlags flags() const override { return sr::ComponentFlags::Tick; }
  
    void tick() override;

    sr::EventSubPtr registerKey(sf::Keyboard::Key _key, FInputCallback&& _func);
    sr::EventSubPtr registerKey(sf::Keyboard::Key _key, sr::InputEvent _ev, FInputCallback&& _func);

private:


    struct InputKeyHandler {
        sr::InputEvent eventType;
        TKeyDispatcher dispatcher;

        InputKeyHandler(sr::InputEvent _event = sr::InputEvent::None) : eventType(_event) { ; }
    };

    struct InputKeyState {
        sf::Keyboard::Key key;
        sr::InputKeyState keyState;

        InputKeyState(sf::Keyboard::Key _key = sf::Keyboard::Key::Unknown, sr::InputKeyState _state = sr::InputKeyState::None) 
            : key(_key)
            , keyState(_state)
        { }
    };

    std::map<sf::Keyboard::Key, std::vector<InputKeyHandler>> mHandlers;
    
    std::map<sf::Keyboard::Key, size_t> mKeyStateMap;
    std::vector<InputKeyState> mKeyStates;
};

#define Input (CInputComponent::contextComponent())