#pragma once

#include <SFML/Graphics.hpp>

class IDrawBatch  : public sf::Drawable {
public:
    virtual size_t type() const = 0;
    IDrawBatch(int32_t _depth) : mDepth(_depth) { ; }
    virtual ~IDrawBatch() = default;

    virtual bool isBatchCompatible(IDrawBatch* _other) const {
        return _other->type() == type();
    }
    
    virtual void flush(sf::RenderTarget& _target) = 0;

    virtual int32_t depth() const { return mDepth; }
    bool compareDepth(const IDrawBatch& _rhs) {
        return depth() < _rhs.depth();
    }

private:
    void depth(int32_t _depth) { mDepth = _depth; }
    int32_t mDepth;

    friend class CDrawBatcher;
};

template <typename TDrawable>
struct TDrawBatchData {
    sr::PtrVector<TDrawable> drawableList;
};

template <typename TDrawable, typename TData = TDrawBatchData<TDrawable>>
class TDrawBatch : public IDrawBatch {
public:
    static size_t sType() { return reinterpret_cast<size_t>(&sType); }
    size_t type() const { return mTypeId; }
    TDrawBatch(int32_t _depth) : IDrawBatch(_depth) {
        mTypeId = TDrawBatch<TDrawable>::sType();
    }

    void batch(const std::shared_ptr<TDrawable>& _drawable) {
        mBatchData.drawableList.push_back(_drawable);
    }

    void flush(sf::RenderTarget& _target) {
        _target.draw(*this);
        mBatchData.drawableList.clear();
    }
    
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override { 
        for (auto& drawable : mBatchData.drawableList) {
            target.draw(*drawable);
        }
    }

    bool isCompatible(const std::shared_ptr<TDrawable>& _other) const {
        return true;
    }

private:
    size_t mTypeId;
    TDrawBatchData<TDrawable> mBatchData;
};
