#pragma once

#include "./vendor/rtree/RTree.h"

#include "scenecomponent.h"

class IEntityCollidable {
public:

private:
};

class CEntityCollisions : public ISceneComponent {
public:

private:
    RTree<IEntityCollidable*, int, 2> mSceneTree;
};
