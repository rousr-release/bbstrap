#pragma once

#include "core.h"

#include <SFML/Graphics.hpp>
#include "resources.h"

template<>
class TResource<sf::Texture> : public IResource {
public:
    TResource(const std::string& _filePath, const sf::IntRect& _area = sf::IntRect()) {
        mResource.loadFromFile(_filePath, _area);
    }

    const sf::Texture& operator*() const { return mResource; }
    sf::Texture& operator*() { return mResource; }

private:
    sf::Texture mResource;
};

