#include "filesystem.h"

//#define BOOST_FILESYSTEM_NO_DEPRECATED
//#include <boost/filesystem.hpp>

#define MAX_PATH (256)

ContextComponentDefine(CFileSystem)

CFileSystem::CFileImpl::~CFileImpl() {
    if (mFileSystem) {
        mFileSystem->remove(this);
    }
}

std::shared_ptr<CFile> CFileSystem::open(const std::string& _filePath, bool _createIfMissing) {
    auto newFile(std::make_shared<CFileImpl>(_filePath, _createIfMissing, this));
    mOpenFiles[_filePath] = newFile;
    return newFile;
}

void CFileSystem::remove(CFile* _file) {
    auto itFile(mOpenFiles.find(_file->filepath()));
    if (itFile == mOpenFiles.end())
        return;

    mOpenFiles.erase(itFile);
}

std::string CFileSystem::getExecutablePath() const {
    char buffer[MAX_PATH];

#if defined WIN32
    GetModuleFileNameA(NULL, buffer, MAX_PATH);
#elif defined __GNUC__

#if defined __APPLE__
    uint32_t buffSize(MAX_PATH);
    _NSGetExecutablePath(buffer, &buffSize);
#else 
    char procId[MAX_PATH];
    memset(procId, 0, MAX_PATH);
    sprintf(procId, "/proc/%d/exe", getpid());
    readlink(procId, buffer, MAX_PATH);
#endif
#endif 
    std::string buffString(buffer);
    std::string::size_type pos(buffString.find_last_of("\\/"));

    return buffString.substr(0, pos);
}

//
/////@funtion FileSystemPath
/////@desc return a filesystem entry's data, file info / folder
/////@param {const std::string&} _path   path 
/////@returns {bool} true if the path is valid
//bool OutsideTheBox::FileSystemPath(const std::string& _path, CGMLBuffer& _buffer) {
//    boost::filesystem::path path(_path);
//    if (!boost::filesystem::exists(path))
//        return false;
//
//    try {
//        FileSystemWritePath(_path, _buffer);
//    } catch (boost::filesystem::filesystem_error&) {
//        return false; // nothing
//    }
//
//    return true;
//}
//
/////@function FileSystemListDirectory(_path)
/////@desc Writes directory entries to a buffer
/////@param {const std::string&} _path
//void OutsideTheBox::FileSystemListDirectory(const std::string& _path, CGMLBuffer& _buffer) {
//    _buffer.Seek(0);
//    boost::filesystem::path path(_path);
//    if (!boost::filesystem::exists(path)) {
//        _buffer.Write<int8_t>(-1);
//        return;
//    }
//
//    try {
//        for (boost::filesystem::directory_iterator fileIter(path), end; fileIter != end; ++fileIter) {
//            // write entry data
//            auto& f(*fileIter);
//            auto p(f.path());
//
//            _buffer.Write<int8_t>(1);
//            FileSystemWritePath(p, _buffer);
//        }
//    } catch (boost::filesystem::filesystem_error&) {
//        _buffer.Write<int8_t>(-1);
//    }
//    _buffer.Write<int8_t>(0);
//}
//
/////@function FileSystemWritePath(_path)
/////@desc write a _path entry to a rousr buffer
/////@param {boost::filesystem::path} _path   `_path` to write
//void OutsideTheBox::FileSystemWritePath(const boost::filesystem::path& _path, CGMLBuffer& _buffer) {
//    auto fws = [&_buffer](const wchar_t* _wstring) {
//        size_t numChars(0);
//        char stringBuff[MAX_PATH];
//        wcstombs_s(&numChars, stringBuff, _wstring, MAX_PATH);
//        return std::string(stringBuff);
//    };
//
//    auto canonPath(boost::filesystem::canonical(_path));
//    auto filePath(fws(canonPath.make_preferred().c_str()));
//    auto genericPath(_path.generic_path());
//
//    struct stat stat_data;
//    memset(&stat_data, 0, sizeof(stat_data));
//    auto ret(stat(filePath.c_str(), &stat_data));
//    bool accessible(ret >= 0);
//
//    _buffer.Write<std::string>(fws(_path.filename().c_str()));
//    _buffer.Write<std::string>(_path.has_extension() ? fws(_path.extension().c_str()) : "");
//    _buffer.Write<std::string>(filePath);
//    _buffer.Write<std::string>(fws(genericPath.c_str()));
//    _buffer.Write<int8_t>(accessible ? 1 : 0);
//    _buffer.Write<uint32_t>(stat_data.st_size);
//
//    _buffer.Write<int8_t>(boost::filesystem::is_directory(_path) ? 1 : 0);
//    _buffer.Write<int8_t>(accessible && boost::filesystem::is_empty(_path) ? 1 : 0);
//    _buffer.Write<int8_t>(boost::filesystem::is_regular_file(_path) ? 1 : 0);
//    _buffer.Write<int8_t>(boost::filesystem::is_symlink(_path) ? 1 : 0);
//    _buffer.Write<int8_t>(boost::filesystem::is_other(_path) ? 1 : 0);
//
//    _buffer.Write<int8_t>(_path.is_absolute() ? 1 : 0);
//    _buffer.Write<int8_t>(_path.is_relative() ? 1 : 0);
//
//    _buffer.Write<uint64_t>(stat_data.st_ctime); // created time
//    _buffer.Write<uint64_t>(stat_data.st_atime); // access time
//    _buffer.Write<uint64_t>(stat_data.st_mtime); // modified time   
//}