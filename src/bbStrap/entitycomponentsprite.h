#pragma once

#include "entitycomponent.h"

#include "../bbStrap/bounds.h"
#include "../bbStrap/resources.h"

#include <SFML/Graphics.hpp>

namespace sf { class Texture; }
class CSpriteRenderer;
class CSpriteBatcher;

class EntityComponentSprite : public IEntityComponent {
public:

    void onConstruct() override;
    void onConstruct(std::shared_ptr<TResource<sf::Texture>> _texture);

    const Bounds<int>& bounds() const { return mBounds; }
    const int depth() const { return mDepth; }
    const std::shared_ptr<sf::Texture>& texture() const { return mTexture; }
        
    sf::Sprite& sprite() { return *mSprite; }

protected:

private:
    Bounds<int> mBounds;
    int mDepth = 0;
    std::shared_ptr<sf::Texture> mTexture;
        
    std::shared_ptr<sf::Sprite> mSprite;
};


#include "drawbatch.h"

template <>
struct TDrawBatchData<EntityComponentSprite> {
    sr::PtrVector<EntityComponentSprite> drawableList;
    
    // state
    std::shared_ptr<sf::Texture> mTexture;
    std::shared_ptr<sf::Shader> mShader;
    sf::Transform mTransform;
    sf::BlendMode mBlendMode = sf::RenderStates::Default.blendMode;

    // renderable
    sf::VertexBuffer mBuffer;
};

template<>
void TDrawBatch<EntityComponentSprite>::batch(const std::shared_ptr<EntityComponentSprite>& _sprite) {
    mBatchData.drawableList.push_back(_sprite);
}

template<>
void TDrawBatch<EntityComponentSprite>::flush(sf::RenderTarget& _target) {
    _target.draw(*this);
    mBatchData.drawableList.clear();
}

template<> 
void TDrawBatch<EntityComponentSprite>::draw(sf::RenderTarget& _target, sf::RenderStates _states) const  {
    /*_states.shader = mBatchData.mShader.get();
    _states.texture = mBatchData.mTexture.get();
    _states.transform *= mBatchData.mTransform;
    _states.blendMode = mBatchData.mBlendMode;

    target.draw(mBatchData.mBuffer, _states);
*/

    for (auto& drawable : mBatchData.drawableList) {
        _target.draw(drawable->sprite());
    }
}



/*
void flush(sf::RenderTarget& _target) {
    _target.draw(*this);
}
*/