#include "drawbatcher.h"

#include "drawbatch.h"
#include "debug.h"

void CDrawBatcher::draw() {
    ProfilerCapture("Batches:", mBatches.size());

    for (auto& depthBatch : mBatches) {
        for (auto& batch : depthBatch.second) {
            batch->flush(*mRenderTarget);
            mFreeBatches[batch->type()].push_back(batch);
        }
    }

    mBatches.clear();
    mActiveBatch.reset();
}