#pragma once


template <typename T> 
class TParent {
public:
    using ParentBase = TParent<T>;

    virtual void addChild(T* _child) {
        _child->removeFromParent();
        mChildren.push_back(_child);
        _child->setParent(this);
    }

    virtual void removeChild(T* _child) {
        removeChildAt(findChild(_child));
    }

    virtual void removeChildAt(size_t _childIndex) {
        if (_childIndex < mChildren.size())
            mChildren.erase(mChildren.begin() + _childIndex);
    }

    virtual size_t findChild(T* _child) const {
        auto itChild(std::find(mChildren.begin(), mChildren.end(), _child));
        if (itChild == mChildren.end()) {
            return std::numeric_limits<size_t>::max();
        }

        return itChild - mChildren.begin();
    }

    void setParent(T* _parent) { mParent = _parent; }
    T* parent() { return mParent; }

protected:
    virtual void removeFromParent() {
        mParent = nullptr;
    }

private:
    std::vector<T*> mChildren;
    T* mParent;
};

template <typename T>
class TSharedParent : public sr::virtual_enable_shared_from_this<TSharedParent<T>> {

public:
    using ParentBase = TSharedParent<T>;

    virtual void addChild(std::shared_ptr<T> _child) {
        _child->removeFromParent();
        mChildren.push_back(_child);
        _child->setParent(TSharedParent<T>::shared_from_this());
    }

    virtual void removeChild(std::shared_ptr<T> _child) {
        removeChildAt(findChild(_child));
    }

    virtual void removeChildAt(size_t _childIndex) {
        if (_childIndex < mChildren.size()) {
            mChildren.erase(mChildren.begin() + _childIndex);
        }
    }

    virtual size_t findChild(std::shared_ptr<T> _child) const {
        auto itChild(std::find(mChildren.begin(), mChildren.end(), _child));
        if (itChild == mChildren.end()) {
            return std::numeric_limits<size_t>::max();
        }

        return itChild - mChildren.begin();
    }

    void setParent(const std::shared_ptr<TSharedParent<T>>& _parent) { mParent = _parent; }
    std::shared_ptr<T> parent() { return std::dynamic_pointer_cast<T>(mParent.lock()); }
    
protected:
    virtual void removeFromParent() { 
        mParent.reset(); 
    }
    
private:
    std::vector<std::shared_ptr<T>> mChildren;
    std::weak_ptr<TSharedParent<T>> mParent;
};