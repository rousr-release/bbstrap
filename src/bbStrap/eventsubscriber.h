#pragma once

#include "core.h"

class IEventDispatcher;

class IEventSubscriber {
protected:
    IEventSubscriber(IEventDispatcher* _dispatcher) : mDispatcher(_dispatcher) { ; }
    virtual ~IEventSubscriber();

    void resetDispatcher() { mDispatcher = nullptr; }

private:
    IEventDispatcher* mDispatcher = nullptr;
    friend class IEventDispatcher;
};

template <typename TFuncSig, typename TSub> class TEventDispatcher;
template <typename TFuncSig>
class TEventSubscriber : public IEventSubscriber {
public:
    using TDispatcher = TEventDispatcher<TFuncSig, TEventSubscriber<TFuncSig>>;
    using TFunc = std::function<TFuncSig>;

    TEventSubscriber(IEventDispatcher* _dispatcher, const TFunc& _func) : IEventSubscriber(_dispatcher), mFunction(_func) { ; }

    template <typename ...Args>
    void call(Args&&... _args) const {
        mFunction(std::forward<Args>(_args)...);
    }

    template <typename TRet, typename ...Args>
    TRet callReturn(Args&&... _args) const {
        return mFunction(std::forward<Args>(_args)...);
    }

    void callLamba(std::function<void(std::function<TFuncSig>)> _func) const {
        _func(mFunction);
    }

protected:
    void resetDispatcher() { mDispatcher = nullptr; }

private:
    TFunc mFunction;

    TDispatcher* mDispatcher;
    friend TDispatcher;
};
