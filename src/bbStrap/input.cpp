#include "input.h"

#include "debug.h"

ContextComponentDefine(CInputComponent)

void CInputComponent::tick() {

    auto doKeyEvent = [this](sf::Keyboard::Key _key, sr::InputEvent _ev) {
        const auto& handlers(mHandlers[_key]);
        const TKeyDispatcher* dispatcher(nullptr);
        for (const auto& handler : handlers) {
            if (handler.eventType == _ev) {
                dispatcher = &(handler.dispatcher);
                break;
            }
        }

        if (dispatcher == nullptr) {
            return;
        }

        // todo: if callbacks are empty we can remove this handler
        
        dispatcher->dispatchLambda([&_key, &_ev](const auto& _sub) {
            return _sub.callReturn<sr::InputReply>(_key, _ev) == sr::InputReply::Pass;
        });
    };

    for (auto& keyState : mKeyStates) {
        bool isCurrentlyDown(sf::Keyboard::isKeyPressed(keyState.key));
        bool wasDown((keyState.keyState & sr::InputKeyState::Down) == sr::InputKeyState::Down);
        
        sr::InputKeyState newState(isCurrentlyDown ? sr::InputKeyState::Down : sr::InputKeyState::Up);
        if (isCurrentlyDown != wasDown) { 
            newState |= sr::InputKeyState::JustChanged; 
            keyState.keyState = newState;
            doKeyEvent(keyState.key, isCurrentlyDown ? sr::InputEvent::Pressed : sr::InputEvent::Released);
        }
        
        keyState.keyState = newState;
        doKeyEvent(keyState.key, isCurrentlyDown ? sr::InputEvent::Down : sr::InputEvent::Up);
    }
}

sr::EventSubPtr CInputComponent::registerKey(sf::Keyboard::Key _key, FInputCallback&& _func) {
    return registerKey(_key, sr::InputEvent::DefaultRegister, std::move(_func));
}

sr::EventSubPtr CInputComponent::registerKey(sf::Keyboard::Key _key, sr::InputEvent _ev, FInputCallback&& _func) {
    std::vector<InputKeyHandler>& handlerList(mHandlers[_key]);
    InputKeyHandler* handler(nullptr);

    for (auto& keyHandler : handlerList) {
        if (keyHandler.eventType == _ev) {
            handler = &keyHandler;
            break;
        }
    }

    if (handler == nullptr) {
        handlerList.emplace_back(_ev);
        handler = &handlerList.back();
    }

    auto newSub(handler->dispatcher.subscribe(_func));

    // create a new keystate if necessary
    auto itKeyState(mKeyStateMap.find(_key));
    if (itKeyState == mKeyStateMap.end()) {
        mKeyStateMap[_key] = mKeyStates.size();
        mKeyStates.emplace_back(_key, sf::Keyboard::isKeyPressed(_key) ? sr::InputKeyState::Down : sr::InputKeyState::Up);
    }

    return newSub;
}
//
//void CInputComponent::unregisterKey(sf::Keyboard::Key _key, CInputComponent::InputHandle _handle) {
//    bool releaseKey(true);
//    
//    std::vector<size_t> eraseHandlers;
//    auto itHandler(mHandlers.find(_key));
//    if (itHandler != mHandlers.end()) {
//        std::vector<InputKeyHandler>& keyHandlers(itHandler->second);
//        size_t handlerIndex(0);
//        for (auto& handler : keyHandlers) {
//            size_t callbackIndex(0);
//            for (auto& callback : handler.callbacks) {
//                if (callback == _handle) {
//                    size_t callbackIndex(static_cast<size_t>(_handle & 0xFFFF));
//                    mCallbacks[callbackIndex] = nullptr;
//                    mAvailableCallbackIndex.push(callbackIndex);
//                    break;
//                }
//                ++callbackIndex;
//            }
//
//            if (callbackIndex < handler.callbacks.size()) {
//                handler.callbacks.erase(handler.callbacks.begin() + callbackIndex);
//                if (handler.callbacks.empty())
//                    eraseHandlers.push_back(handlerIndex);
//            }
//            handlerIndex++;
//        }
//
//        for (size_t handlerIndex : eraseHandlers) {
//            keyHandlers.erase(keyHandlers.begin() + handlerIndex);
//        }
//
//        releaseKey = keyHandlers.empty();
//    }
//     
//    // stop watching a keystate if this was the last key
//    if (releaseKey) {
//        auto itKeyState(mKeyStateMap.find(_key));
//        if (itKeyState != mKeyStateMap.end()) {
//            size_t keyStateIndex(itKeyState->second);
//            mKeyStates.erase(mKeyStates.begin() + keyStateIndex);
//        }
//    }
//}