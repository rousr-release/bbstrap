#pragma once

#include "./vendor/imgui-sfml/imgui-SFML.h"
#include "./vendor/imgui/imgui.h"


#include "eventdispatcher.h"

#include "corecontext.h"
#include "corecontextcomponent.h"

#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Drawable.hpp>


class CBBImGui : public TContextComponentStatic<CBBImGui>, public sf::Drawable {
public:
    virtual sr::ComponentFlags flags() const { return sr::ComponentFlags::Tick | sr::ComponentFlags::Draw; }
    using Base::Base;
    ~CBBImGui();

    void initialize() override;
    void tick() override;
    void draw() override;

    void draw(sf::RenderTarget& _target, sf::RenderStates _states) const;

    sr::EventSubPtr onImGui(const std::function<void()>& _func);
    void onDraw(const std::function<void(CBBImGui&)>& _func);
    void clearOnDraw();

    int32_t depth() const { return 1000; }

private:
    sf::Clock mDelta;

    ImGuiMouseCursor mPrevCursor;

    TEventDispatcher<void()> mOnImGui;
    std::function<void(CBBImGui&)> mOnDraw;

    sr::EventSubs mEvents;
};

#define bbImGui (CBBImGui::contextComponent())