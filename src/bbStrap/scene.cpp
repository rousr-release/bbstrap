#include "scene.h"

CScene::CScene() {
    mRoot = createComponent<SceneRoot>(this);
    
    componentLoop = [this](const std::function<void(const std::shared_ptr<ISceneComponent>&)>&& _func) {
        for (const auto& component : mComponents)
            _func(component);
    };
}

void CScene::onComponentCreated(const std::shared_ptr<ISceneComponent>& _component) {
    _component->setScene(this);
}

void CScene::tick() {
    componentLoop([](auto& _component) { _component->pretick(); });
    componentLoop([](auto& _component) { _component->tick(); });
    componentLoop([](auto& _component) { _component->posttick(); });
}

void CScene::draw() {
    componentLoop([](auto& _component) { _component->predraw(); });
    componentLoop([](auto& _component) { _component->draw(); });
    componentLoop([](auto& _component) { _component->postdraw(); });
}
