#include "entity.h"

#include "entitycomponent.h"

namespace entityInternal {
    size_t CEntityPhaseSubscriber::getIndexInParent() const {
        auto entity(mEntity.lock());
        if (entity == nullptr) {
            return std::numeric_limits<size_t>::max();
        }

        auto parent(entity->parent());
        if (parent == nullptr) {
            return 0;
        }

        return parent->findChild(entity);
    }

    bool EntityPhaseSubscriberOrder::operator()(const CEntityPhaseSubscriber* _left, const CEntityPhaseSubscriber* _right) {
        return _left->getIndexInParent() < _right->getIndexInParent();
    }
}

void IEntity::onComponentCreated(const std::shared_ptr<IEntityComponent>& _component) {
    _component->mEntity = shared_from_this<IEntity>();
}

void IEntity::dispatchEvent(const std::string& _event, IEntityMsg& _msg) const {
    mEntityEvents.dispatchLambda(_event, [&_msg](const auto& _sub) {
        _msg.lastReply = _sub.callReturn<EntityReply>(_msg);
        if (_msg.lastReply == EntityReply::Consume)
            return true;
        
        return false;
    });
}

// let a child subscribe to an entityphase update
sr::EventSubPtr IEntity::subscribePhase(EEntityPhase _phase, const std::shared_ptr<IEntity>& _child) {
    // subscribe to parent's phase of this if haven't 
    int phaseIndex(static_cast<int>(_phase));
    if (mPhaseSubscriptions[phaseIndex] == nullptr) {
        auto parent(parent());
        if (parent != nullptr) {
            mPhaseSubscriptions[phaseIndex] = parent->subscribePhase(_phase, shared_from_this<IEntity>());
        }
    }

    return mEntityPhases.subscribe(_phase, [_child, _phase]() { _child->dispatchPhase(_phase); }, _child);
}

void IEntity::subscribePhase(EEntityPhase _phase, const std::shared_ptr<IEntityComponent>& _component, const std::function<void()>& _func) {
    int phaseIndex(static_cast<int>(_phase));
    if (mPhaseSubscriptions[phaseIndex] == nullptr) {
        auto parent(parent());
        if (parent != nullptr)
            mPhaseSubscriptions[phaseIndex] = parent->subscribePhase(_phase, shared_from_this<IEntity>());
    }

    mComponentSubscriptions[_component.get()].push_back(mEntityComponentPhases.subscribe(_phase, _func));
}

void IEntity::addChild(std::shared_ptr<IEntity> _child) {
    ParentBase::addChild(_child);
    _child->setScene(&scene());
}


void IEntity::dispatchPhase(EEntityPhase _phase) {
    mEntityComponentPhases.dispatch(_phase);
    mEntityPhases.dispatch(_phase);
}