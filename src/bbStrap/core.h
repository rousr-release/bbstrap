#pragma once

#include <iostream>
#include <memory>

#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include <stack>
#include <deque>
#include <queue>

#include <algorithm>
#include <functional>

#include <inttypes.h>
#include <type_traits>

#include "sfml.h"

#include "sr.h"
