#pragma once

#include "core.h"

#include "eventsubscriber.h"

#include "classtype.h"

namespace sr {
    using EventSubPtr = std::shared_ptr<IEventSubscriber>;
    using EventSubs = std::vector<EventSubPtr>;
}

class IEventSubscriber;

class IEventDispatcher {
public:
    virtual ~IEventDispatcher() = default;

protected:
    void resetDispatcher(IEventSubscriber* _sub) {
        _sub->resetDispatcher();
    }

    virtual void remove(IEventSubscriber* _ptr) { ; }
    friend class IEventSubscriber;
};

template <typename TFuncSig, typename TSubscriber = TEventSubscriber<TFuncSig>>
class TEventDispatcher : public IEventDispatcher {
public:
    using TFunc = std::function<TFuncSig>;
    using TSubCRef = const TSubscriber&;

    virtual ~TEventDispatcher() {
        for (auto& sub : mSubscribers) {
            resetDispatcher(sub);
        }

        mSubscribers.clear();
    }

    std::shared_ptr<IEventSubscriber> subscribe(const TFunc& _func) {
        std::shared_ptr<TSubscriber> subscriber(std::make_shared<TSubscriber>(this, _func));
        mSubscribers.push_back(subscriber.get());
        return std::reinterpret_pointer_cast<IEventSubscriber>(subscriber);
    }
    
    template <typename ...Args>
    void dispatch(Args&&... _args) const {
        for (auto& subscriber : mSubscribers) {
            subscriber->call(std::forward<Args>(_args)...);
        }
    }

    void dispatchLambda(const std::function<bool(TSubCRef)>& _handler) const {
        for (auto& subscriber : mSubscribers) {
            if (_handler(*subscriber)) {
                return;
            }
        }
    }
    
    template <typename TRet, typename ...Args>
    TRet dispatchReturn(const TRet& _returnOnVal, Args&&... _args) const {
        TRet ret;
        for (auto& subscriber : mSubscribers) {
            TRet _ret(subscriber->callReturn<TRet>(std::forward<Args>(_args)...));
            if (_ret == _returnOnVal)
                break;
        }
        return _ret;
    }

    template <typename TRet, typename ...Args>
    TRet dispatchReturnLambda(const std::function<bool(const TRet&)> _func, Args&&... _args) const {
        TRet ret;
        for (auto& subscriber : mSubscribers) {
            ret = subscriber->callReturn<TRet>(std::forward<Args>(_args)...);
            if (_func(ret))
                break;
        }
        return _ret;
    }

    bool empty() const {
        return mSubscribers.empty();
    }
    
protected:
    void remove(IEventSubscriber* _ptr) override {
        auto itSub(std::find_if(mSubscribers.begin(), mSubscribers.end(), [_ptr](TSubscriber* _elem) { return _elem == _ptr; }));
        if (itSub != mSubscribers.end())
            mSubscribers.erase(itSub);
    }

    friend TSubscriber;
private:
    std::vector<TSubscriber*> mSubscribers;
};