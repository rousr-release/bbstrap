#pragma once

#include "entity.h"
#include "scenecomponent.h"

class SceneRoot : public IEntity, public ISceneComponent {
public:
    void onConstruct(CScene* _scene);
    
    ////
    // Phases
    void pretick();
    void tick();
    void posttick();

    void predraw();
    void draw();
    void postdraw();
    // ~Phases

private:

};