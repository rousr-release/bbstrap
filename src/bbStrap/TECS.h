#pragma once

template <typename TParent, typename TComponentBase>
class TECS {
public:
    template <typename TComponent>
    std::shared_ptr<TComponent> getComponent() {
        auto itComponent(std::find_if(mComponents.begin(), mComponents.end(), [](auto& _component) {
            return std::dynamic_pointer_cast<TComponent>(_component) != nullptr;
        }));

        if (itComponent == mComponents.end()) {
            return nullptr;
        }           

        return std::dynamic_pointer_cast<TComponent>(*itComponent);
    }

    template <typename TComponent, typename ...Args> 
    std::shared_ptr<TComponent> createComponent(Args&&... _args) { 
        auto newComponent(std::make_shared<TComponent>());
        mComponents.push_back(newComponent);
        onComponentCreated(mComponents.back()); 
        newComponent->onConstruct(std::forward<Args>(_args)...);
        return newComponent;
    }
protected:
    virtual void onComponentCreated(const std::shared_ptr<TComponentBase>& _component) { ; }

private:
    sr::PtrVector<TComponentBase> mComponents;

    friend TParent;
};