#pragma once

#if defined(DISABLE_PROFILING)
#define ProfilerCapture(...)
#else
#define ProfilerCapture(captureName, val) { ; }
#endif