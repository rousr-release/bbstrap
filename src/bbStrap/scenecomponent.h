#pragma once

class CScene;

class ISceneComponent {
public:
    ISceneComponent() { }
    virtual ~ISceneComponent() { ; }

    virtual void onConstruct() { ; }

    virtual void pretick() { ; }
    virtual void tick() { ; }
    virtual void posttick() { ; }

    virtual void predraw() { }
    virtual void draw() { }
    virtual void postdraw() { }

    CScene* scene() { return mParent; }

protected:
    void setScene(CScene* _parent) { mParent = _parent; }

private:
    CScene* mParent = nullptr;

    friend class CScene;
};