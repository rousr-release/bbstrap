#pragma once

#include "eventdispatchersorted.h"
#include "eventdispatchermulti.h"
#include "eventsubscriber.h"

class IEntity;

enum class EEntityPhase : int32_t {
    Pretick = 0,
    Tick,
    Posttick,

    Predraw,
    Draw,
    Postdraw,

    Num,
};

namespace entityInternal {
    class CEntityPhaseSubscriber : public TEventSubscriber<void()> {
    public:
        CEntityPhaseSubscriber(IEventDispatcher* _dispatcher, const TEventSubscriber::TFunc& _func, const std::shared_ptr<IEntity>& _entity) : TEventSubscriber(_dispatcher, _func), mEntity(_entity) { ; }
        size_t getIndexInParent() const;

    private:
        std::weak_ptr<IEntity> mEntity;
    };

    struct EntityPhaseSubscriberOrder {
        bool operator()(const CEntityPhaseSubscriber* _left, const CEntityPhaseSubscriber* _right);
    };

    using TEntityPhaseDispatcher = TSortedEventDispatcher<void(), EntityPhaseSubscriberOrder, CEntityPhaseSubscriber>;
    using EntityPhaseDispatcher  = TMultiEventDispatcher<EEntityPhase, void(), CEntityPhaseSubscriber, TEntityPhaseDispatcher>;

}