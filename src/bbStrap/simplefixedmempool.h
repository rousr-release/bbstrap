#pragma once

#include "core.h"

template <typename T>
class CSimpleFixedMemPool {
public:

    T* alloc() {
        if (mAvailable.empty()) {
            mPool.emplace_back(std::make_unique<T>());
            mPoolMap[mPool.back().get()] = mPool.size();
            return mPool.back().get();
        }

        size_t poolIndex(mAvailable.back());
        mAvailable.pop_back();

        return mPool[poolIndex].get();
    }

    void free(T* _rawPtr) {
        auto itPoolIndex(mPoolMap.find(_rawPtr));
        if (itPoolIndex == mPoolMap.end())
            return;

        mAvailable.push_back(itPoolIndex->second);
    }

    void reset(bool _releaseMem = false) {
        mAvailable.clear();
        if (_releaseMem) {
            mPoolMap.clear();
            mPool.clear();
            return;
        }

        for (size_t index(0), end(mPool.size()); index < end; ++index)
            mAvailable.push_back(index);
    }

    size_t allocations() {
        return mPool.size();
    }

private:
    std::vector<size_t> mAvailable;
    sr::UPtrVector<T> mPool;
    std::map<T*, size_t> mPoolMap;
};
