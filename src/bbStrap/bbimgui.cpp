#include "corecontext.h"

#include "sfml.h"
#include <SFML/Graphics.hpp>

#include "./vendor/imgui-sfml/imgui-SFML.h"

#include "bbimgui.h"

ContextComponentDefine(CBBImGui)

void CBBImGui::initialize() {
    Base::initialize();
    sf::VertexArray imGuiArray;

    ImGui::SFML::Init(*CoreCtx.renderWindow());

    mEvents.push_back(CoreCtx.subscribeEvent([](sf::Event& _event) {
        ImGui::SFML::ProcessEvent(_event);
        return false;
    }));

    clearOnDraw();
}

CBBImGui::~CBBImGui() {
    ImGui::SFML::Shutdown();
}

void CBBImGui::tick() {
    
    ImGui::SFML::Update(*CoreCtx.renderWindow(), mDelta.restart());

    mOnImGui.dispatch();

    ImGuiMouseCursor currentCursor = ImGui::GetMouseCursor();
    if (currentCursor != mPrevCursor) {
        mPrevCursor = currentCursor;
        switch (currentCursor) {
        case ImGuiMouseCursor_Arrow:      CoreCtx.setMouseCursor(sf::Cursor::Arrow); break;
        case ImGuiMouseCursor_TextInput:  CoreCtx.setMouseCursor(sf::Cursor::Text); break;
        case ImGuiMouseCursor_ResizeAll:  CoreCtx.setMouseCursor(sf::Cursor::SizeAll); break;
        case ImGuiMouseCursor_ResizeNS:   CoreCtx.setMouseCursor(sf::Cursor::SizeVertical); break;
        case ImGuiMouseCursor_ResizeEW:   CoreCtx.setMouseCursor(sf::Cursor::SizeHorizontal); break;
        case ImGuiMouseCursor_ResizeNESW: CoreCtx.setMouseCursor(sf::Cursor::SizeBottomLeftTopRight); break;
        case ImGuiMouseCursor_ResizeNWSE: CoreCtx.setMouseCursor(sf::Cursor::SizeTopLeftBottomRight); break;
        case ImGuiMouseCursor_Hand:       CoreCtx.setMouseCursor(sf::Cursor::Hand); break;
        default: break;
        }
    }
}

void CBBImGui::draw() {
    mOnDraw(*this);
}

void CBBImGui::draw(sf::RenderTarget& _target, sf::RenderStates _states) const {
    ImGui::SFML::Render(_target);
}

sr::EventSubPtr CBBImGui::onImGui(const std::function<void()>& _func) {
    return mOnImGui.subscribe(_func);
}

void CBBImGui::onDraw(const std::function<void(CBBImGui&)>& _func) {
    mOnDraw = _func;
}

void CBBImGui::clearOnDraw() {
    mOnDraw = [](CBBImGui& _this) { CoreCtx.renderWindow()->draw(_this); };
}