#pragma once

#include "corecontextcomponent.h"

class IResource {
public:
    virtual ~IResource() = default;
};

template <typename T>
class TResource : public IResource {
public:
    const T& operator*() const { return mResource; }
    T& operator*() { return mResource; }

private:
    T mResource;
};


class ResourceInvalidType : public std::exception {
public:
    ResourceInvalidType(const std::string& _typeText) : std::exception((std::string("resource type doesn't match resource for ") + _typeText).c_str()) { ; }
};

class CResources : public TContextComponentStatic<CResources> {
    static const std::string NoFilePath;
public:
    using Base::Base;
    CResources(std::shared_ptr<CCoreContext> _context, const std::string& _resourceRoot, const std::string& _filePath) : Base(_context), mRoot(_resourceRoot), mResourceFilePath(_filePath) { ; }


    void initialize() override {
        Base::initialize();
        loadResources(mResourceFilePath);
    }

    sr::ComponentFlags flags() const override { return sr::ComponentFlags::Tick; }
    
    template <typename T, typename... Args>
    std::shared_ptr<TResource<T>> get(const std::string& _tag, const std::string& _filePath = NoFilePath, Args&&... _args) {
        auto itResource(mResources.find(_tag));
        std::shared_ptr<TResource<T>> resource;
        if (itResource == mResources.end()) {
            if (_filePath == NoFilePath) {
                return nullptr;
            }

            resource = std::make_shared<TResource<T>>(_filePath, std::forward<Args>(_args)...);
        } else {
            resource = std::dynamic_pointer_cast<TResource<T>>(itResource->second);
            if (resource == nullptr) {
                throw ResourceInvalidType(_tag);
            }
        }

        return resource;
    
    }

    void loadResources(const std::string& _filePath);

protected:
    std::map<std::string, std::shared_ptr<IResource>> mResources;
    std::string mRoot;
    std::string mResourceFilePath;
};

#define Resources (CResources::contextComponent())