#include "eventdispatcher.h"

IEventSubscriber::~IEventSubscriber() {
    if (mDispatcher != nullptr) {
        mDispatcher->remove(this);
    }
}
