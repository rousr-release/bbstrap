#pragma once

#include "core.h"
class IEntityComponent;

enum class EntityReply {
    Pass = 0,
    Consume,
};

class IEntityMsg {
public:
    std::shared_ptr<IEntityComponent> source;
    EntityReply lastReply;

    IEntityMsg(const std::shared_ptr<IEntityComponent>& _source, EntityReply _lastReply = EntityReply::Pass) : source(_source), lastReply(_lastReply) { ; }

    // std::function<void(const std::shared_ptr<IEntityComponent>&)> handleSource;    // call with self
};
