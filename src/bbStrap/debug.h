#pragma once

#include "corecontextcomponent.h"

#include "profiler.h"

#define DISABLE_PROFILING

class CDebugComponent : public TContextComponentStatic<CDebugComponent> {
public:
    using Base::Base;
    void log(const std::string& _log);
};

#define Debug (CDebugComponent::contextComponent())