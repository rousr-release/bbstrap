#pragma once

template <typename T>
class TObjectPool {

	template<typename... Args>
	std::shared_ptr<T> Withdraw(Args&&... constructorArgs) {
		if (!mAvailableIndices.empty()) {
			size_t index(mAvailableIndices.pop_front());
			std::shared_ptr<T> object(mObjects[index]);
			new (object.get()) T(std::forward<Args>(constructorArgs)...);
			return object;
		} 
			
		return std::make_shared<T>(std::forward<Args>(constructorArgs)...);
	}

	void Deposit(std::shared_ptr<T> _t) {
		_t->~T();

	}

private:
	std::vector<std::shared_ptr<T>> mObjects;
	std::deque<size_t> mAvailableIndices;

};