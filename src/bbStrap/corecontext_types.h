#pragma once

#include "core.h"

namespace sr {
    enum class ComponentFlags : uint32_t {
        None = 0,

        Tick = (1 << 0),
        Draw = (1 << 1),
         
        Default = None,
    };

    template <typename EClass> inline EClass operator& (const EClass& _left, const EClass& _right) { using T = std::underlying_type_t<EClass>; return  static_cast<EClass>(static_cast<T>(_left) & static_cast<T>(_right)); }
    template <typename EClass> inline EClass operator&= (EClass& _left, const EClass& _right) { using T = std::underlying_type_t<EClass>; _left = static_cast<EClass>(static_cast<T>(_left) & static_cast<T>(_right)); return _left; }

    template <typename EClass> inline EClass operator| (const EClass& _left, const EClass& _right) { using T = std::underlying_type_t<EClass>; return  static_cast<EClass>(static_cast<T>(_left) | static_cast<T>(_right)); }
    template <typename EClass> inline EClass operator|= (EClass& _left, const EClass& _right) { using T = std::underlying_type_t<EClass>; _left = static_cast<EClass>(static_cast<T>(_left) | static_cast<T>(_right)); return _left; }
}