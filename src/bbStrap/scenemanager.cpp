#include "scenemanager.h"

#include "../bbStrap/scene.h"

//////////
// CScene management
TSceneHandle CSceneManager::addScene(const std::string& _sceneName, std::shared_ptr<CScene> _newScene, bool _active, bool _front ) {
    if (_sceneName.length() == 0 || _sceneName == "") {
        throw std::exception("Bad scene name - must give a name");
    }

    auto itFind(mSceneMap.find(_sceneName));
    if (itFind != mSceneMap.end() && mSceneHandles.find(itFind->second) != mSceneHandles.end()) {
        throw std::exception("Adding scene with the same name to the list.");
    }
    
	TSceneHandle newHandle(mNextHandle++);
	mSceneHandles[newHandle] = _newScene;

    if (_front) {
        mScenes.insert(std::begin(mScenes), newHandle);
    } else {
        mScenes.push_back(newHandle);
    }

    mSceneMap[_sceneName] = newHandle;
    if (_active)
        mCurrentScene = newHandle;
    
    return newHandle;
}

void CSceneManager::removeScene(TSceneHandle _handle) {
	auto itFind(mSceneHandles.find(_handle));
	if (itFind == mSceneHandles.end()) 
		return;

    mSceneHandles.erase(itFind);
    auto itHandleIndex(std::find(mScenes.begin(), mScenes.end(), _handle));
    if (itHandleIndex != mScenes.end()) {
        mScenes.erase(itHandleIndex);
    }

    for (auto iter(std::begin(mSceneMap)), iterEnd(std::end(mSceneMap)); iter != iterEnd; ++iter) {
        if (iter->second == _handle) {
            mSceneMap.erase(iter);
            break;
        }
    }
}

bool CSceneManager::setActiveScene(TSceneHandle _handle) {
    size_t newScene(sceneIndex(_handle));
    if (newScene == InvalidSceneIndex)
        return false;

    mCurrentScene = newScene;
    return true;
}

std::shared_ptr<CScene> CSceneManager::scene(TSceneHandle _handle) const {
    auto itFind(mSceneHandles.find(_handle));
    if (itFind != mSceneHandles.end())
        return itFind->second;

    return nullptr;
}

TSceneHandle CSceneManager::sceneHandle(const std::string& _sceneName) {
	auto itFind(mSceneMap.find(_sceneName));
	if (itFind == mSceneMap.end())
		return false;

    return itFind->second;
}

TSceneHandle CSceneManager::sceneFromIndex(size_t _index) const {
    if (_index >= mScenes.size())
        return InvalidSceneHandle;

    return mScenes[_index];
}

size_t CSceneManager::sceneIndex(TSceneHandle _handle) const {
    auto itIndexFind(std::find(mScenes.begin(), mScenes.end(), _handle));
    if (itIndexFind != mScenes.end()) {
        return itIndexFind - mScenes.begin();
    }

    return InvalidSceneIndex;
}

TSceneHandle CSceneManager::activeSceneHandle() const {
    return mCurrentScene;
}

TSceneHandle CSceneManager::nextScene() {
    mCurrentScene = std::min(++mCurrentScene, (mScenes.size() - 1));
    return mScenes[mCurrentScene];
}

TSceneHandle CSceneManager::prevScene() {
    mCurrentScene = std::max(--mCurrentScene, static_cast<size_t>(0));
    return mScenes[mCurrentScene];
}

void CSceneManager::shiftScene(TSceneHandle _handle, int32_t _index) {
    size_t insertIndex(0);
    size_t myIndex(0);
    for (size_t it(0), itEnd(mScenes.size()); it < itEnd; ++it) {
        if (mScenes[it] == _handle) {
            myIndex = it;
            if (static_cast<int>(it) + _index >= 0) {
                insertIndex = it - _index;
            }
            break;
        }
    }

    insertIndex = std::min(mScenes.size() - 2, insertIndex);
    if (insertIndex == myIndex)
        return;
    
    mScenes.insert(mScenes.begin() + insertIndex, _handle);
    if (insertIndex < myIndex)
        myIndex++;
    mScenes.erase(mScenes.begin() + myIndex);
}

void CSceneManager::tickScene() { 
    TSceneHandle tempScene(0);
    while (mOldScene != mCurrentScene) {
        auto oldScene(scene(mOldScene));
        auto currentScene(scene(mCurrentScene));

        if (oldScene)
            oldScene->exit(currentScene);

        mOldScene = mCurrentScene;
        currentScene->enter(oldScene);
    }

    std::shared_ptr<CScene> currentScene(mSceneHandles[mOldScene]);
    currentScene->tick();
}

void CSceneManager::drawScene() { 
    std::shared_ptr<CScene> currentScene(mSceneHandles[mOldScene]);
    currentScene->draw();
}

//////////
