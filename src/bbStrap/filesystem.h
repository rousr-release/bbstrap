#pragma once

#include "corecontextcomponent.h"

#include "file.h"

// namespace boost { namespace filesystem { class path; class directory_iterator; } }

class CFileSystem : public TContextComponentStatic<CFileSystem> {
public:
    using Base::Base;

    std::shared_ptr<CFile> open(const std::string& _filePath, bool _createIfMissing = false);

    std::string getExecutablePath() const;

    // bool path(const std::string& _path, CGMLBuffer& _out);
    // void listDirectory(const std::string& _path, CGMLBuffer& _out);
    // void writePath(const boost::filesystem::path& _path, CGMLBuffer& _out);



private:
    std::map<std::string, std::weak_ptr<CFile>> mOpenFiles;

private:
    void remove(CFile* _file);

    class CFileImpl : public CFile {
    public:
        CFileImpl(const std::string& _filePath, bool _createIfMissing, CFileSystem* _fileSystem) : CFile(_filePath, _createIfMissing), mFileSystem(_fileSystem) { ; }
        ~CFileImpl();
    private:
        void clearFilesystem() { mFileSystem = nullptr; }
        CFileSystem *mFileSystem;

        friend class CFileSystem;
    };
    friend class CFileImpl;
};

#define FileSystem (CFileSystem::contextComponent())