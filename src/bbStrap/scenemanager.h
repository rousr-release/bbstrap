#pragma once

#include "corecontextcomponent.h"

class CScene;

using TSceneHandle = size_t;
const size_t InvalidSceneIndex(static_cast<size_t>(~0));
const size_t InvalidSceneHandle(static_cast<size_t>(~0));

class CSceneManager : public CCoreContextComponent {
public:
    using CCoreContextComponent::CCoreContextComponent;
    sr::ComponentFlags flags() const override { return sr::ComponentFlags::Tick | sr::ComponentFlags::Draw; }

    virtual ~CSceneManager() { }

    //////////
    // CScene management
	TSceneHandle addScene(const std::string& _sceneName, std::shared_ptr<CScene> _newScene, bool _active = false, bool _front = false);
    void removeScene(TSceneHandle _handle);
    bool setActiveScene(TSceneHandle _sceneHandle);

    std::shared_ptr<CScene> scene(TSceneHandle _handle) const;
    TSceneHandle sceneHandle(const std::string& _sceneName);
    TSceneHandle sceneFromIndex(size_t _index) const;
    size_t sceneIndex(TSceneHandle _handle) const;

	TSceneHandle activeSceneHandle() const;

    TSceneHandle nextScene();
    TSceneHandle prevScene();
	
	// change the order the scene appears
	void shiftScene(TSceneHandle _handle, int32_t _index);

    virtual void tickScene();
    virtual void drawScene();

    //////////
    
    void tick() override { tickScene(); }
    void draw() override { drawScene(); }

protected:
    

private:
    std::map<std::string, size_t> mSceneMap;
	std::map<size_t, std::shared_ptr<CScene>> mSceneHandles;
    std::vector<size_t> mScenes;
    TSceneHandle mCurrentScene = InvalidSceneHandle;
    TSceneHandle mOldScene     = InvalidSceneHandle;

	TSceneHandle mNextHandle = 0;
};