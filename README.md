# bbStrap

bbStrap is a lightweight framework built on top of SFML to provide "Game Engine"-like features.

Features include:

*   Entity+Component system
*   SFML Drawable batching
*   Input Event system
*   Scene Management
*   ImGui support
*   Resource Management

---

### Dependencies

**Internal**
Expects the following rousr projects to be found relative to this project:

* rousr
    * engine (../../engine)
        * SFML (../../engine/SFML) - git@git.rou.sr:rousr/sfml.git
    * games
        * bbStrap [this]

**External**

*   [SFML 2.5](https://www.sfml.org)
*   [R-Tree](https://github.com/nushoin/RTree) - nushoin @ github
*   [RapidJSON](https://github.com/Tencent/rapidjson)
*   [ImGui](https://github.com/ocornut/imgui)
*   [stb](https://github.com/nothings/stb)
*   [ImGui-SFML](https://github.com/eliasdaler/imgui-sfml)